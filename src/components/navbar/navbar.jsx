import React from "react";
import "./navbar.css";

function Navbar() {
    return (
            <div className = "navbar-container ">
                <ul className = "navbar">
                    <li className = "nav-item">
                        Home
                    </li>
                    <li className = "nav-item">
                        About Me
                    </li>
                    <li className = "nav-item">
                        Contact
                    </li>
                </ul>
            </div>
    );
}

export default Navbar;