import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLinkedin } from "@fortawesome/free-brands-svg-icons"
import { faGitlab } from "@fortawesome/free-brands-svg-icons"
import { faInstagram } from "@fortawesome/free-brands-svg-icons"


import "./icon.css"

function Icon() {
    return (
    <div className ="iconAwsome">           
        <h2><FontAwesomeIcon icon={faLinkedin}/></h2>
        <h2><FontAwesomeIcon icon={faGitlab}/></h2>
        <h2><FontAwesomeIcon icon={faInstagram}/></h2>
    </div>
    )
}

export default Icon;