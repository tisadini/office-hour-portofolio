import './main.css';
import Navbar from "./components/navbar/navbar";
import Profile from "./pages/profile";
import RightSde from "./right/right";
import Icon from "./components/icon/icon";

function App() {
  return (
    <>
    <Navbar />
      <div className="main-center">
        <Profile />
        <RightSde />
      </div>
      <div className= "icon">
        <Icon />
      </div>
  </>
  );
}

export default App;
